package algorithms.preprocessing;

import java.util.Arrays;

/**
 * This class offers methods for restructuring objects.
 */

public class Preprocessor {
 
    /**
     * Constructor does nothing
     * @param None
     */
    public Preprocessor() {
    }
 
    /**
     * Convert a string of numbers to an array of integers
     * @param stringOfNumbers A string of space-delimited integers
     * @return numbers An array of integers
     */
    public static int[] stringArrayToIntArray(String[] stringOfNumbers) {
        int[] numbers = new int[stringOfNumbers.length];
        for (int i = 0; i < stringOfNumbers.length; i++) {
            numbers[i] = Integer.parseInt(stringOfNumbers[i]);
        }
        return numbers;
    }

    /**
     * Print a one-dimensional array of integers
     * @param numbers A one-dimensional array of integers
     * @return Nothing
     */
    public static void printArray(int[] numbers) {
        String sorted = Arrays.toString(numbers);
        System.out.println(sorted);
    }  
}
