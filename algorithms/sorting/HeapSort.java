package algorithms.sorting;

import algorithms.preprocessing.Preprocessor;

/**
 * The HeapSort algorithm is a comparative, iterative, in-place 
 * sorting alogorithm. The first step is to build a max-heap data 
 * structure; this is an array satisfying the max-heap property 
 * i.e. array[i] >= array[2 * (i + 1)] and array[i] >= array[2 * (i + 2)]. 
 * The next step iteratively sorts the integers in order of decreasing 
 * array-index k i.e. during the kth iteration, swap the value  
 * array[0] with the element array[n-k] and then heapify to preverve
 * the max-heap property. The heapify subroutine is recursive. The 
 * result is a sorted array of integers in order of increasing value.
 *
 * @author Steven Ghoussain
 * @version 1.0 
 * @since 04/03/16
 */
public class HeapSort extends ComparativeSort {

    /**
     * Initialise instance variables 
     * @param unsortedNumbers An integer array of numbers for sorting
     * @return Nothing
     */
    public HeapSort(int[] unsortedNumbers) {
        numbers = unsortedNumbers;
    }

    @Override
    public void sort() {
        int auxiliaryIndex = numbers.length-1;
        buildMaxHeap();
        for(int i = 1; i < numbers.length; i++) {
            swap(0, auxiliaryIndex);
            auxiliaryIndex--;
            maxHeapify(0, auxiliaryIndex);
        }
    }

    /**
     * Build max-heap
     * @param None
     * @return None
     */
    private static void buildMaxHeap() {		 
        int middleIndex = (int) ((numbers.length/2) - 1);

        /* all values a[k] for k > middleIndex are trivially heapified.
         * begin heapifying from middleIndex
         */  
        for(int i = middleIndex; i >= 0; i--) {
            maxHeapify(i, numbers.length-1);
        }
    }

    /**
     * Max-heapify: maintain a heap with root at index i
     * @param currentIndex Root index
     * @param lastIndex Largest index in the heap
     * @return Nothing
     */
    private static void maxHeapify(int currentIndex, int lastIndex) {
        int index = currentIndex;
        int leftChildIndex = (2 * currentIndex) + 1; 		    
        int rightChildIndex = (2 * currentIndex) + 2; 	         

        /* obtain index of the largest value amongst parent, left child and right child */
        if((leftChildIndex <= lastIndex) && (numbers[leftChildIndex] > numbers[currentIndex])) {  
            index = leftChildIndex;
        } 
        if((rightChildIndex <= lastIndex) && (numbers[rightChildIndex] > numbers[index])) {
            index = rightChildIndex;
        }

        if(index != currentIndex) {
            /** max-heap property is violated; swap values and recurs */
            swap(currentIndex, index);			    
            maxHeapify(index, lastIndex);		          
        }
    }

    /**
     * Swap two elements of an array.
     * @param index1 A legal array-index
     * @param index2 A legal array-index
     * @return: Nothing
     */
    private static void swap(int index1, int index2) {
        int copy = numbers[index1];
        numbers[index1] = numbers[index2];
        numbers[index2] = copy;
    }

    public static void main(String[] args) { 
        /* read a string of space-delimited numbers from the command line */
        if (args.length != 1) {
            System.out.println("Enter a string of space-delimited integers");
            System.exit(0);
        }
        String[] stringArray = args[0].split(" ");    
          
        /* convert string of numbers to an array of numbers */
        Preprocessor preprocessor = new Preprocessor();                        
        int[] numbers = new int[stringArray.length];                          
        numbers = preprocessor.stringArrayToIntArray(stringArray);

        /* sort numbers and print result */
        HeapSort heapSort = new HeapSort(numbers);
        heapSort.sort();
        preprocessor.printArray(numbers);
    } 
} 
