package algorithms.sorting;

public interface Sort {
 
    /* Sort using an implementable type of comparative sorting algorithm. */
    public void sort();

}
