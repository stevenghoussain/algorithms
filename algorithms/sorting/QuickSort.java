package algorithms.sorting;

import algorithms.preprocessing.Preprocessor;

/**
 * The QuickSort algorithm is a comparative, recursive, in-place 
 * sorting alogorithm, that divides, conquers and combines. 
 * Partition a subarray about the endpoint of the array, sort the 
 * partitioned point and then recurs on the left and right subarrays.
 * The result is a sorted array of integers in order of increasing value.
 *
 * @author Steven Ghoussain
 * @version 1.0 
 * @since 04/03/16
 */
public class QuickSort extends ComparativeSort {

    /**
     * Initialise instance variables.
     * @param unsortedNumbers An integer array of numbers for sorting
     * @return Nothing
     */
    public QuickSort(int[] unsortedNumbers) {
        numbers = unsortedNumbers;
    }

    @Override
    public void sort() {
        sort(0, (numbers.length - 1));
    }

    /**
     * Sort an array of numbers using Quick Sort.
     * @param leftIndex Lower index bound
     * @param rightIndex Upper index bound
     * @return Nothing
     */
    private static void sort(int leftIndex, int rightIndex) {
        if(leftIndex < rightIndex) {

            /* partition the array and sort partition point */
            int q = partition(leftIndex, rightIndex); 
        
            /* recursively sort left and right subarrays */
            sort(leftIndex, (q - 1));  
            sort((q + 1), rightIndex); 
        }
    }

    /**
     * Partition a subarray array[leftIndex:rightIndex] about the partition 
     * point array[rightIndex]. Sort the partition point. Return the index 
     * of the sorted partition point. 
     * Loop Invariants: (let i denote a partition point)
     *     1. if leftIndex <= k <= i then a[k] <= array[i].
     *     2. if i+1 <= k <= rightIndex-1 then array[k] > array[i].
     *     3. if k = rightIndex then array[k] = array[rightIndex].
     * @param leftIndex Lower index of a subarray
     * @param rightIndex Index of the partition point
     * @return The index of the sorted partition point
     */
    private static int partition(int leftIndex, int rightIndex) {
        int i = leftIndex - 1;
        int auxiliaryIndex;

        /* partition array about the partition point */
        for(auxiliaryIndex = leftIndex; auxiliaryIndex < rightIndex; auxiliaryIndex++) {
            if(numbers[auxiliaryIndex] <= numbers[rightIndex]) { 
                i++;
                swap(i, auxiliaryIndex);
            }
        }

        /* sort partition point */
        swap((i + 1), rightIndex); 
 
        /* return index of sorted partition point */
        return (i + 1);	   
 }

    /**
     * Swap two elements of an array.
     * @param index1 A legal array-index
     * @param index2 A legal array-index
     * @return: Nothing
     */
    private static void swap(int index1, int index2) {
        int copy = numbers[index1];
        numbers[index1] = numbers[index2];
        numbers[index2] = copy;
    }

    public static void main(String[] args) { 
        /* read a string of space-delimited numbers from the command line */
        if (args.length != 1) {
            System.out.println("Enter a string of space-delimited integers");
            System.exit(0);
        }
        String[] stringArray = args[0].split(" ");    
          
        /* convert string of numbers to an array of numbers */
        Preprocessor preprocessor = new Preprocessor();                        
        int[] numbers = new int[stringArray.length];                          
        numbers = preprocessor.stringArrayToIntArray(stringArray);

        /* sort numbers and print result */
        QuickSort quickSort = new QuickSort(numbers);
        quickSort.sort();    
        preprocessor.printArray(numbers);
    } 
}

//     Asymptotic Complexity:
//         Expected Time complexity: Theta(n*lg(n)) [solve the recurrence f(n) = f(n/2) + kn].
//             Worst case run time is quadratic Theta(n^2).
//             Worst case occurs for input in descending order.
//         Space complexity: O(1), in-place.
