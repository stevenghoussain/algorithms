package algorithms.sorting;

import algorithms.preprocessing.Preprocessor;

/**
 * The MergeSort algorithm is a comparative, recursive, sorting algorithm,
 * that divides, conquers and combines. Divide the array into left and right
 * subarrays of approximately equal length, recursively sort subarrays and 
 * merge. The result is a sorted array of integers in order of increasing value.
 *
 * @author Steven Ghoussain
 * @version 1.0 
 * @since 04/03/16
 */
public class MergeSort extends ComparativeSort {

    protected static int[] sorted;

    /**
    * Initialise instance variables.
    * @param unsortedNumbers An integer array of numbers for sorting
    * @return Nothing
    */
    public MergeSort(int[] unsortedNumbers) {
        numbers = unsortedNumbers;
    }
   
    @Override
    public void sort() {
        sorted = sort(0, (numbers.length - 1));
    }
 
    /**
     * Sort an array of numbers using Merge Sort.
     * @param leftIndex Lower array-index bound
     * @param rightIndex Upper array-index bound
     * @return Sorted array between index bounds
     */
    private int[] sort(int leftIndex, int rightIndex) {
        if(rightIndex == leftIndex) { 

            /* array has length 1: base case */
            return (new int[] {numbers[leftIndex]});
        } else {  

              /* array has length > 1: recurs */
              int k = (int) (rightIndex - leftIndex)/2; 
              return merge(sort(leftIndex, (leftIndex + k)), 
                           sort((leftIndex + k + 1), rightIndex)
              );
          }
    }

    /** 
     * Merge two sorted arrays.
     * @param array1 A sorted array of integers
     * @param array2 A sorted array of integers
     * @return merged The result of merging the two sorted arrays
     */
    private static int[] merge(int[] array1, int[] array2) {
        int length = array1.length + array2.length;
        int[] merged = new int[length];
        int i = 0;
        int j = 0;
        int k = 0;
        while(k < length) {
            if(i == array1.length) { 

                /* preempt ArrayIndexOutOfBounds Exception */
                merged[k] = array2[j];
                j++;
            } else if (j == array2.length) { 

                  /* preempts ArrayIndexOutOfBounds Exception */
                  merged[k] = array1[i];
                  i++;
              } else if (array1[i] < array2[j]) {
                    merged[k] = array1[i];
                    i++;
                } else { 
                      merged[k] = array2[j];
                      j++;
                  }
            k++;
        }
        return merged;
    }

    public static void main(String[] args) { 
        /* read a string of space-delimited numbers from the command line */
        if (args.length != 1) {
            System.out.println("Enter a string of space-delimited integers");
            System.exit(0);
        }
        String[] stringArray = args[0].split(" ");    
          
        /* convert string of numbers to an array of numbers */
        Preprocessor preprocessor = new Preprocessor();                        
        int[] numbers = new int[stringArray.length];                          
        numbers = preprocessor.stringArrayToIntArray(stringArray);

        /* sort numbers and print result */
        int[] sorted = new int[numbers.length];
        MergeSort mergeSort = new MergeSort(numbers);
        mergeSort.sort();
        preprocessor.printArray(mergeSort.sorted);
    } 
} 

//    Asymptotic Complexity:
//        Time complexity: O(n*lg(n)) asymptotic cost: solve the recurrence f(n) = 2f(n/2) + k*n
//        where k*n is the overall cost of the merging phase. Thus:
//            f(n) = constant*n + k*n*lg(n) =
//            O(n*lg(n))
//        Space complexity: O(n*lg(n)) linearithmic.
//            Can merge sort in linear space (O(n), an extra array of length n/2), 
//            but implementation is easier at linearithmic cost.
