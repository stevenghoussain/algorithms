package algorithms.sorting;

import algorithms.preprocessing.Preprocessor;

/**
 * The InsertionSort algorithm is a comparative, iterative, in-place 
 * sorting alogorithm. Subarrays are sorted iteratively subarrays in 
 * order of increasing index i.e. sort the value array[k] in the subarray 
 * array[0:k] for increasing index k. During the kth iteration, the Linear 
 * Search algorithm finds the sorted array-index 0 <= k' <= k for value 
 * A[k] and inserts it into the correct slot in the subarray array[0:k].  
 * The result is a sorted array of integers in order of increasing value.
 *
 * @author Steven Ghoussain
 * @version 1.0 
 * @since 04/03/16
 */
public class InsertionSort extends ComparativeSort {

    /**
     * Initialise instance variables 
     * @param unsortedNumbers An integer array of numbers for sorting
     * @return Nothing
     */
     public InsertionSort(int[] unsortedNumbers) { 
        numbers = unsortedNumbers;
     }
  
    @Override
    public void sort() {
        for(int rightIndex = 1; rightIndex < numbers.length; rightIndex++) { 
            insert(rightIndex);
            /* outer loop invariant: subarray array[0...(rightIndex-1)] is sorted */
        }
    }

    /**
     * Insert value array[j] into its sorted place in subarray array[0:j]
     * using the Linear Search algorithm.
     * @param i An array-index to store the 
     * @
     */
    private static void insert(int j) {
        int i = j - 1;
        int copy = numbers[i];

        while((j >= 0) && (numbers[j] > numbers[i])) { 
            numbers[j + 1] = numbers[j]; 			
            j--;	          
            /* inner loop invariant: the last (i-j) elements in subarray a[0...i] 
             * are sorted.
             */		
        } 
  
        if(j >= 0) {
            numbers[j + 1] = copy; 			
        } else { 
            numbers[0] = copy;         
          }
    }

    public static void main(String[] args) { 
        /* read a string of space-delimited numbers from the command line */
        if (args.length != 1) {
            System.out.println("Enter a string of space-delimited integers");
            System.exit(0);
        }
        String[] stringArray = args[0].split(" ");    
          
        /* convert string of numbers to an array of numbers */
        Preprocessor preprocessor = new Preprocessor();                        
        int[] numbers = new int[stringArray.length];                          
        numbers = preprocessor.stringArrayToIntArray(stringArray);

        /* sort numbers and print result */
        InsertionSort insertionSort = new InsertionSort(numbers);
        insertionSort.sort();
        preprocessor.printArray(numbers);
    } 
}      

//     Asymptotic Complexity:
//         Comparisons are expensive: O(n^2) worst case [not theta(n^2) because best case is linear] 
//         Worst case occurs when array is in descending order of value
