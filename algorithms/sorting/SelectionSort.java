package algorithms.sorting;

import algorithms.preprocessing.Preprocessor;

/**
 * The Selection Sort algorithm is a comparative, iterative, in-place 
 * sorting algorithm. Integer values are sorted iteratively in order of 
 * increasing global size i.e. sort the kth largest value in the array 
 * for increasing k. The loop invariant: array[0:k] is a sorted subarray 
 * of the k smallest values in the entire array. The result is a sorted 
 * array of integers in order of increasing value.
 *
 * @author Steven Ghoussain
 * @version 1.0
 * @since 04/03/16
 */ 
public class SelectionSort extends ComparativeSort {

    /**
    * Initialise instance variables 
    * @param unsortedNumbers An integer array of numbers for sorting
    * @return Nothing
    */
    public SelectionSort(int[] unsortedNumbers) {
        numbers = unsortedNumbers;
    } 
   
    public void sort() {
        for (int i = 0; i < numbers.length - 1; i++) {
            sortNext(i);
        }
    } 

    /**
     * Sort the kth largest integer for index k using the Linear Search
     * algorithm. This method maintains the loop invariant: array[0:k] 
     * is a sorted subarray of the k smallest values in the entire array.
     * @param index A legal array-index 
     * @return Nothing
     */ 
    private static void sortNext(int index) {
        int temporaryIndex = index;
        int temporarySmallestValue = numbers[index];
        
        /* n - (i+1) comparisons */
        for(int i = (index + 1); i < numbers.length; i++) { 
            if(numbers[i] < temporarySmallestValue) {
                temporaryIndex = i;
                temporarySmallestValue = numbers[i]; 
            }
        }
        swap(index, temporaryIndex);
    }

    /**
     * Swap two elements of an array.
     * @param index1 A legal array-index
     * @param index2 A legal array-index
     * @return: Nothing
     */
    private static void swap(int index1, int index2) {
        int copy = numbers[index1];
        numbers[index1] = numbers[index2];
        numbers[index2] = copy;
    } 

    public static void main(String[] args) { 
        /* read a string of space-delimited numbers from the command line */
        if (args.length != 1) {
            System.out.println("Enter a string of space-delimited integers");
            System.exit(0);
        }
        String[] stringArray = args[0].split(" ");

        /* convert string of numbers to an array of numbers */
        Preprocessor preprocessor = new Preprocessor();                        
        int[] numbers = new int[stringArray.length];                          
        numbers = preprocessor.stringArrayToIntArray(stringArray);

        /* sort numbers and print result */
        SelectionSort selectionSort = new SelectionSort(numbers);
        selectionSort.sort();
        preprocessor.printArray(numbers);
    } 
} 


//     Asymptotic Complexity:
//         Time complexity: O(n^2):n*(n-1)/2 comparisons + n-1 swaps
//         Space complexity: O(1) constant, in-place.
